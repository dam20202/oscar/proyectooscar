<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "videojuego".
 *
 * @property int $codigo_videojuego
 * @property int|null $codigo_desarrollador
 * @property int|null $codigo_distribuidor
 * @property string|null $nombre
 * @property string|null $fecha_lanzamiento
 * @property string|null $descrpicion
 * @property string|null $SO
 * @property string|null $procesador
 * @property string|null $memoria
 * @property string|null $grafica
 * @property string|null $almacenamiento
 *
 * @property AnalistaVideojuego $analistaVideojuego
 * @property BibliotecaVideojuego $bibliotecaVideojuego
 * @property Dlc $dlc
 * @property Genero $genero
 * @property UsuarioVideojuego $usuarioVideojuego
 * @property Version $version
 * @property Desarrollador $codigoDesarrollador
 * @property Distribuidor $codigoDistribuidor
 */
class Videojuego extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'videojuego';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_videojuego'], 'required'],
            [['codigo_videojuego', 'codigo_desarrollador', 'codigo_distribuidor'], 'integer'],
            [['fecha_lanzamiento'], 'safe'],
            [['nombre'], 'string', 'max' => 30],
            [['descrpicion'], 'string', 'max' => 50],
            [['SO', 'procesador', 'memoria', 'grafica', 'almacenamiento'], 'string', 'max' => 20],
            [['codigo_desarrollador'], 'unique'],
            [['codigo_distribuidor'], 'unique'],
            [['nombre'], 'unique'],
            [['codigo_videojuego'], 'unique'],
            [['codigo_desarrollador'], 'exist', 'skipOnError' => true, 'targetClass' => Desarrollador::className(), 'targetAttribute' => ['codigo_desarrollador' => 'codigo_desarrollador']],
            [['codigo_distribuidor'], 'exist', 'skipOnError' => true, 'targetClass' => Distribuidor::className(), 'targetAttribute' => ['codigo_distribuidor' => 'codigo_distribuidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_videojuego' => 'Codigo Videojuego',
            'codigo_desarrollador' => 'Codigo Desarrollador',
            'codigo_distribuidor' => 'Codigo Distribuidor',
            'nombre' => 'Nombre',
            'fecha_lanzamiento' => 'Fecha Lanzamiento',
            'descrpicion' => 'Descrpicion',
            'SO' => 'So',
            'procesador' => 'Procesador',
            'memoria' => 'Memoria',
            'grafica' => 'Grafica',
            'almacenamiento' => 'Almacenamiento',
        ];
    }

    /**
     * Gets query for [[AnalistaVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnalistaVideojuego()
    {
        return $this->hasOne(AnalistaVideojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[BibliotecaVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBibliotecaVideojuego()
    {
        return $this->hasOne(BibliotecaVideojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[Dlc]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDlc()
    {
        return $this->hasOne(Dlc::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[Genero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenero()
    {
        return $this->hasOne(Genero::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[UsuarioVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioVideojuego()
    {
        return $this->hasOne(UsuarioVideojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[Version]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVersion()
    {
        return $this->hasOne(Version::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[CodigoDesarrollador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDesarrollador()
    {
        return $this->hasOne(Desarrollador::className(), ['codigo_desarrollador' => 'codigo_desarrollador']);
    }

    /**
     * Gets query for [[CodigoDistribuidor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDistribuidor()
    {
        return $this->hasOne(Distribuidor::className(), ['codigo_distribuidor' => 'codigo_distribuidor']);
    }
}
