<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "version".
 *
 * @property int|null $codigo_videojuego
 * @property string|null $version
 * @property int $codigo_version
 * @property float|null $precio
 * @property string|null $contenido
 *
 * @property Videojuego $codigoVideojuego
 */
class Version extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'version';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_videojuego', 'codigo_version'], 'integer'],
            [['codigo_version'], 'required'],
            [['precio'], 'number'],
            [['version'], 'string', 'max' => 30],
            [['contenido'], 'string', 'max' => 50],
            [['codigo_videojuego'], 'unique'],
            [['codigo_version'], 'unique'],
            [['codigo_videojuego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['codigo_videojuego' => 'codigo_videojuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_videojuego' => 'Codigo Videojuego',
            'version' => 'Version',
            'codigo_version' => 'Codigo Version',
            'precio' => 'Precio',
            'contenido' => 'Contenido',
        ];
    }

    /**
     * Gets query for [[CodigoVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }
}
