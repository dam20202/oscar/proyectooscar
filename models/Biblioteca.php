<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "biblioteca".
 *
 * @property int $codigo_biblioteca
 * @property int|null $codigo_usuario
 * @property string|null $nombre_usuario
 *
 * @property Usuario $codigoUsuario
 * @property Usuario $nombreUsuario
 * @property BibliotecaVideojuego $bibliotecaVideojuego
 * @property Etiquetas $etiquetas
 */
class Biblioteca extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'biblioteca';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_biblioteca'], 'required'],
            [['codigo_biblioteca', 'codigo_usuario'], 'integer'],
            [['nombre_usuario'], 'string', 'max' => 30],
            [['codigo_usuario'], 'unique'],
            [['codigo_biblioteca'], 'unique'],
            [['codigo_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['codigo_usuario' => 'codigo_usuario']],
            [['nombre_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['nombre_usuario' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_biblioteca' => 'Codigo Biblioteca',
            'codigo_usuario' => 'Codigo Usuario',
            'nombre_usuario' => 'Nombre Usuario',
        ];
    }

    /**
     * Gets query for [[CodigoUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUsuario()
    {
        return $this->hasOne(Usuario::className(), ['codigo_usuario' => 'codigo_usuario']);
    }

    /**
     * Gets query for [[NombreUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreUsuario()
    {
        return $this->hasOne(Usuario::className(), ['nombre' => 'nombre_usuario']);
    }

    /**
     * Gets query for [[BibliotecaVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBibliotecaVideojuego()
    {
        return $this->hasOne(BibliotecaVideojuego::className(), ['codigo_biblioteca' => 'codigo_biblioteca']);
    }

    /**
     * Gets query for [[Etiquetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEtiquetas()
    {
        return $this->hasOne(Etiquetas::className(), ['codigo_biblioteca' => 'codigo_biblioteca']);
    }
}
