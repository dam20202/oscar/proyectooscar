<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "biblioteca_videojuego".
 *
 * @property int|null $codigo_biblioteca
 * @property int|null $codigo_videojuego
 * @property int $codigo_biblioteca_videojuego
 *
 * @property Biblioteca $codigoBiblioteca
 * @property Videojuego $codigoVideojuego
 */
class BibliotecaVideojuego extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'biblioteca_videojuego';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_biblioteca', 'codigo_videojuego', 'codigo_biblioteca_videojuego'], 'integer'],
            [['codigo_biblioteca_videojuego'], 'required'],
            [['codigo_biblioteca'], 'unique'],
            [['codigo_videojuego'], 'unique'],
            [['codigo_biblioteca_videojuego'], 'unique'],
            [['codigo_biblioteca'], 'exist', 'skipOnError' => true, 'targetClass' => Biblioteca::className(), 'targetAttribute' => ['codigo_biblioteca' => 'codigo_biblioteca']],
            [['codigo_videojuego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['codigo_videojuego' => 'codigo_videojuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_biblioteca' => 'Codigo Biblioteca',
            'codigo_videojuego' => 'Codigo Videojuego',
            'codigo_biblioteca_videojuego' => 'Codigo Biblioteca Videojuego',
        ];
    }

    /**
     * Gets query for [[CodigoBiblioteca]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoBiblioteca()
    {
        return $this->hasOne(Biblioteca::className(), ['codigo_biblioteca' => 'codigo_biblioteca']);
    }

    /**
     * Gets query for [[CodigoVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }
}
