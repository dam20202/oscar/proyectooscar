<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $codigo_usuario
 * @property string|null $nombre
 * @property string|null $correo
 * @property string|null $contraseña
 *
 * @property Biblioteca $biblioteca
 * @property Biblioteca[] $bibliotecas
 * @property UsuarioVideojuego $usuarioVideojuego
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_usuario'], 'required'],
            [['codigo_usuario'], 'integer'],
            [['nombre', 'correo', 'contraseña'], 'string', 'max' => 30],
            [['nombre'], 'unique'],
            [['codigo_usuario'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_usuario' => 'Codigo Usuario',
            'nombre' => 'Nombre',
            'correo' => 'Correo',
            'contraseña' => 'Contraseña',
        ];
    }

    /**
     * Gets query for [[Biblioteca]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBiblioteca()
    {
        return $this->hasOne(Biblioteca::className(), ['codigo_usuario' => 'codigo_usuario']);
    }

    /**
     * Gets query for [[Bibliotecas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBibliotecas()
    {
        return $this->hasMany(Biblioteca::className(), ['nombre_usuario' => 'nombre']);
    }

    /**
     * Gets query for [[UsuarioVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioVideojuego()
    {
        return $this->hasOne(UsuarioVideojuego::className(), ['codigo_usuario' => 'codigo_usuario']);
    }
}
