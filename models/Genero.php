<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "genero".
 *
 * @property int|null $codigo_videojuego
 * @property string|null $genero
 * @property int $codigo_genero
 *
 * @property Videojuego $codigoVideojuego
 */
class Genero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'genero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_videojuego', 'codigo_genero'], 'integer'],
            [['codigo_genero'], 'required'],
            [['genero'], 'string', 'max' => 30],
            [['codigo_videojuego'], 'unique'],
            [['genero'], 'unique'],
            [['codigo_genero'], 'unique'],
            [['codigo_videojuego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['codigo_videojuego' => 'codigo_videojuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_videojuego' => 'Codigo Videojuego',
            'genero' => 'Genero',
            'codigo_genero' => 'Codigo Genero',
        ];
    }

    /**
     * Gets query for [[CodigoVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }
}
