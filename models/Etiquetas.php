<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "etiquetas".
 *
 * @property string|null $etiqueta
 * @property int|null $codigo_biblioteca
 * @property int $codigo_etiqueta
 *
 * @property Biblioteca $codigoBiblioteca
 */
class Etiquetas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'etiquetas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_biblioteca', 'codigo_etiqueta'], 'integer'],
            [['codigo_etiqueta'], 'required'],
            [['etiqueta'], 'string', 'max' => 30],
            [['etiqueta'], 'unique'],
            [['codigo_biblioteca'], 'unique'],
            [['codigo_etiqueta'], 'unique'],
            [['codigo_biblioteca'], 'exist', 'skipOnError' => true, 'targetClass' => Biblioteca::className(), 'targetAttribute' => ['codigo_biblioteca' => 'codigo_biblioteca']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'etiqueta' => 'Etiqueta',
            'codigo_biblioteca' => 'Codigo Biblioteca',
            'codigo_etiqueta' => 'Codigo Etiqueta',
        ];
    }

    /**
     * Gets query for [[CodigoBiblioteca]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoBiblioteca()
    {
        return $this->hasOne(Biblioteca::className(), ['codigo_biblioteca' => 'codigo_biblioteca']);
    }
}
