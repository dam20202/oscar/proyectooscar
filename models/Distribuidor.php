<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribuidor".
 *
 * @property int $codigo_distribuidor
 * @property string|null $nombre
 *
 * @property Videojuego $videojuego
 */
class Distribuidor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribuidor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_distribuidor'], 'required'],
            [['codigo_distribuidor'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['codigo_distribuidor'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_distribuidor' => 'Codigo Distribuidor',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Videojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_distribuidor' => 'codigo_distribuidor']);
    }
}
