<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario_videojuego".
 *
 * @property int|null $codigo_usuario
 * @property int|null $codigo_videojuego
 * @property int $codigo_usuario_videojuego
 *
 * @property Usuario $codigoUsuario
 * @property Videojuego $codigoVideojuego
 */
class UsuarioVideojuego extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario_videojuego';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_usuario', 'codigo_videojuego', 'codigo_usuario_videojuego'], 'integer'],
            [['codigo_usuario_videojuego'], 'required'],
            [['codigo_usuario'], 'unique'],
            [['codigo_videojuego'], 'unique'],
            [['codigo_usuario_videojuego'], 'unique'],
            [['codigo_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['codigo_usuario' => 'codigo_usuario']],
            [['codigo_videojuego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['codigo_videojuego' => 'codigo_videojuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_usuario' => 'Codigo Usuario',
            'codigo_videojuego' => 'Codigo Videojuego',
            'codigo_usuario_videojuego' => 'Codigo Usuario Videojuego',
        ];
    }

    /**
     * Gets query for [[CodigoUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUsuario()
    {
        return $this->hasOne(Usuario::className(), ['codigo_usuario' => 'codigo_usuario']);
    }

    /**
     * Gets query for [[CodigoVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }
}
