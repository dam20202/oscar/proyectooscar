<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Videojuego */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="videojuego-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_videojuego')->textInput() ?>

    <?= $form->field($model, 'codigo_desarrollador')->textInput() ?>

    <?= $form->field($model, 'codigo_distribuidor')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_lanzamiento')->textInput() ?>

    <?= $form->field($model, 'descrpicion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'procesador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'memoria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grafica')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'almacenamiento')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
