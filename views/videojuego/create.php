<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Videojuego */

$this->title = 'Create Videojuego';
$this->params['breadcrumbs'][] = ['label' => 'Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videojuego-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
