<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Desarrollador */

$this->title = 'Update Desarrollador: ' . $model->codigo_desarrollador;
$this->params['breadcrumbs'][] = ['label' => 'Desarrolladors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_desarrollador, 'url' => ['view', 'id' => $model->codigo_desarrollador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="desarrollador-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
