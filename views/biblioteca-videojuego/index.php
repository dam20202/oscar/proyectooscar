<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Biblioteca Videojuegos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biblioteca-videojuego-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Biblioteca Videojuego', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_biblioteca',
            'codigo_videojuego',
            'codigo_biblioteca_videojuego',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
