<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dlc */

$this->title = 'Create Dlc';
$this->params['breadcrumbs'][] = ['label' => 'Dlcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dlc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
