<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dlc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dlc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_videojuego')->textInput() ?>

    <?= $form->field($model, 'dlc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_dlc')->textInput() ?>

    <?= $form->field($model, 'precio')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
