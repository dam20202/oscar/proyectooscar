<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Analista */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="analista-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_analista')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
